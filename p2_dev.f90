!MATH96012 Project 2
!This module contains two module variables and three subroutines;
!two of these routines must be developed for this assignment.
!Module variables--
! lr_x: training images, typically n x d with n=784 and d<=15000
! lr_y: labels for training images, d-element array containing 0s and 1s
!   corresponding to images of even and odd integers, respectively.
!lr_lambda: l2-penalty parameter, should be set to be >=0.
!Module routines---
! data_init: allocate lr_x and lr_y using input variables n and d. May be used if/as needed.
! clrmodel: compute cost function and gradient using classical logistic
!   regression model (CLR) with lr_x, lr_y, and
!   fitting parameters provided as input
! mlrmodel: compute cost function and gradient using MLR model with m classes
!   and with lr_x, lr_y, and fitting parameters provided as input

module lrmodel
  implicit none
  real(kind=8), allocatable, dimension(:,:) :: lr_x
  integer, allocatable, dimension(:) :: lr_y
  real(kind=8) :: lr_lambda !penalty parameter

contains

!---allocate lr_x and lr_y deallocating first if needed (used by p2_main)--
! ---Use if needed---
subroutine data_init(n,d)
  implicit none
  integer, intent(in) :: n,d
  integer :: i1
  if (allocated(lr_x)) deallocate(lr_x)
  if (allocated(lr_y)) deallocate(lr_y)
  allocate(lr_x(n,d),lr_y(d))
  !do i1=1,d
  !  lr_x(:,i1)=(/0.2+i1/10,0.4+i1/10,0.6+i1/10/)
  !end do
!  print *, 'hello'
!  print *, lr_x(4,3)
!  lr_y=(/1.0,0.0,0.0,1.0/)
end subroutine data_init


!Compute cost function and its gradient for CLR model
!for d images (in lr_x) and d labels (in lr_y) along with the
!fitting parameters provided as input in fvec.
!The weight vector, w, corresponds to fvec(1:n) and
!the bias, b, is stored in fvec(n+1)
!Similarly, the elements of dc/dw should be stored in cgrad(1:n)
!and dc/db should be stored in cgrad(n+1)
!Note: lr_x and lr_y must be allocated and set before calling this subroutine.
subroutine clrmodel(fvec,n,d,c,cgrad)

  implicit none
  integer, intent(in) :: n,d !training data sizes
  real(kind=8), dimension(n+1), intent(in) :: fvec !fitting parameters
  real(kind=8),dimension(1,n+1) ::fvec2
  real(kind=8), intent(out) :: c !cost
  real(kind=8), dimension(n+1), intent(out) :: cgrad !gradient of cost
  real(kind=8), dimension(1,d) :: a
  real(kind=8),dimension(1,d)::y2
  real(kind=8),dimension(1,n+1) :: cgrad2

  cgrad2(1,:)=cgrad

  !Declare other variables as needed
  !call data_init(n,d)
  fvec2(1,:)=fvec
  y2(1,:)=lr_y
!print *, shape(matmul(fvec2(:,1:n),lr_x)+fvec2(1,n+1))
  a= exp(matmul(fvec2(:,1:n),lr_x)+fvec2(1,n+1))/(1.d0+exp(matmul(fvec2(:,1:n),lr_x)+fvec2(1,n+1)))
!print *, a

!  print *, shape(lr_y)
  !print *, shape(lr_x)
!compute the cost function c
!print *,a(1,:)
c=(-1.d0)*dot_product(y2(1,:),log(a(1,:)+(1.0d-12)))-1.d0*dot_product(1.d0-y2(1,:),log((1.d0-a(1,:))+(1.0d-12)))


c=c+lr_lambda*dot_product(fvec2(1,1:n),fvec2(1,1:n))

!print *,'c=',c

!compute the gradiant of the cost function cgrad

cgrad(n+1)=-1.d0*dot_product(y2(1,:),1.d0-a(1,:))+dot_product(1.d0-y2(1,:),a(1,:))

!print *, 'comment'
!print *, shape(transpose(lr_x))
!print *, shape(lr_x)
cgrad2(:,1:n)=matmul(y2*(1.d0-a),transpose(lr_x))+ matmul((1.d0-y2)*a,transpose(lr_x))


cgrad2(:,1:n)=cgrad2(:,1:n)+2.d0*lr_lambda*fvec2(:,1:n)

!print *, 'pas mal'
cgrad(1:n)=cgrad2(1,1:n)
cgrad2(1,n+1)=cgrad(n+1)
!print *, cgrad

!print *, 'hello'


!print *,'a=',a

  !Add code to compute c and cgrad

end subroutine clrmodel


!!Compute cost function and its gradient for MLR model
!for d images (in lr_x) and d labels (in lr_y) along with the
!fitting parameters provided as input in fvec. The labels are integers
! between 0 and m-1.
!fvec contains the elements of the weight matrix w and the bias vector, b
! Code has been provided below to "unpack" fvec
!The elements of dc/dw and dc/db should be stored in cgrad
!and should be "packed" in the same order that fvec was unpacked.
!Note: lr_x and lr_y must be allocated and set before calling this subroutine.
subroutine mlrmodel(fvec,n,d,m,c,cgrad)

  implicit none
  integer, intent(in) :: n,d,m !training data sizes and number of classes
  real(kind=8), dimension((m-1)*(n+1)), intent(in) :: fvec !fitting parameters
  real(kind=8), intent(out) :: c !cost
  real(kind=8), dimension((m-1)*(n+1)), intent(out) :: cgrad !gradient of cost
  integer :: i1,j1,i2,i3,i4,i5,i6,i7,k
  real(kind=8), dimension(m,n) :: w
  real(kind=8), dimension(m) :: b
  real(kind=8), dimension(m,d) :: a
  real(kind=8), dimension(m,d) :: z
  real(kind=8), dimension(d) :: s
  real(kind=8) :: r
  !Declare other variables as needed

c=0
  !unpack fitting parameters (use if needed)
  do i1=1,n
    j1 = (i1-1)*(m-1)+1
    w(:,i1) = fvec(j1:j1+m-2) !weight matrix
  end do
  b = fvec(m*n+1:m*(n+1)) !bias vector

 z=matmul(w,lr_x)
 z(1,:)=0.d0
 do i7=1,d
   z(:,i7)=z(:,i7)+b

end do

a=exp(z)
s=sum(a,dim=1)
do i2=1,d

  a(:,i2)=a(:,i2)/s(i2)

end do

do i4=1,m
  do k=1,d
    if (lr_y(k)==i4) then
      c=c+log(a(i4,k)+1.0d-12)


    end if

end do
end do

do i5=1,m-1
  do i6=1,n

    !r=lr_lambda*dot_product(w(i5,:),w(i5,:))
    r=lr_lambda*w(i5,i6)*w(i5,i6)
end do
end do

c=(-1.d0)*c+r
  !Add code to compute c and cgrad


end subroutine mlrmodel



end module lrmodel
